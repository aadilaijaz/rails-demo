class ContactController < ApplicationController

    def list
        @contacts = Contact.order('first_name')
    end

    def new
        @contact = Contact.new
    end

    def create
        @contact = Contact.new(contact_params)
        if @contact.save
            redirect_to :action => 'list'
        else
            @contacts = Contact.new
            render :action => 'new'
        end
    end

    def edit
        @contact = Contact.find(params[:id])
    end

    def update
        @contact = Contact.find(params[:id])
        if @contact.update_attributes(contact_params)
            redirect_to :action => 'list'
        else
            @contact = Contact.find(params[:id])
            render :action => 'edit'
        end
    end

    def delete
        Contact.find(params[:id]).destroy
        redirect_to :action => 'list'
    end

    private
        def contact_params
            params.require(:contact).permit(:first_name, :last_name, :phone, :location)
        end

end
