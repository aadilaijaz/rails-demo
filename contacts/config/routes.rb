Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'contact/list'
  get 'contact/new'
  post 'contact/create'
  get 'contact/edit'
  patch 'contact/update'
  get 'contact/delete'

  root 'contact#list'

end
