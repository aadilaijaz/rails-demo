class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.column :first_name, :string, :null => false
      t.column :last_name, :string, :null=> false
      t.column :phone, :integer, :limit => 5, :null => false
      t.column :location, :string
      t.timestamps
    end
  end
end
