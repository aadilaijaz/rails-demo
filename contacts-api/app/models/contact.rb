class Contact < ApplicationRecord
    #validations
    validates_presence_of :first_name, :last_name, :phone
end
