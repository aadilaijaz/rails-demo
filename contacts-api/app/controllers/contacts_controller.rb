class ContactsController < ApplicationController
    before_action :set_contact, only: [:show, :update, :destroy]

    # GET /contacts
    def index
        @contacts = Contact.all
        json_response(@contacts)
    end

    # POST /contacts
    def create
        @contact = Contact.create!(contact_params)
        json_response(@contact, :created)
    end

    # GET /contacts/:id
    def show
        json_response(@contact)
    end

    # POST /contacts/:id
    def update
        @contact.update(contact_params)
        head :no_content
    end

    # DELETE /contacts/:id
    def destroy
        @contact.destroy
        head :no_content
    end

    private

    def set_contact
        @contact = Contact.find(params[:id])
    end

    def contact_params
        params.permit(:first_name, :last_name, :phone, :location)
    end

end
