require 'faker'

FactoryBot.define do
    factory :contact do
        first_name { Faker::Name.first_name }
        last_name { Faker::Name.last_name }
        phone { Faker::Number.number(10) }
        location { Faker::Lorem.word }
    end
end
