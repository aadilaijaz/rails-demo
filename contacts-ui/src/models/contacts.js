var m = require("mithril");


var Contacts = {
    list: [],
    loadList: function(){
        return m.request({
            method: "GET",
            url: "http://localhost:3000/contacts"
        }).then(function(result){
            Contacts.list = result;
        });
    },

    postData: {},
    save: function(){
        return m.request({
            method: "POST",
            url: "http://localhost:3000/contacts",
            data: Contacts.postData
        });
    },

    singleContact: {},
    loadSingle: function(id){
        return m.request({
            method: "GET",
            url: "http://localhost:3000/contacts?id=eq."+id
        }).then(function(result){
            Contacts.singleContact = result[0];
        });
    },

    update: function(){
        return m.request({
            method: "PUT",
            url: "http://localhost:3000/contacts?id=eq."+Contacts.singleContact.id,
            data: Contacts.singleContact
        });
    },

    delete: function(id){
        return m.request({
            method: "DELETE",
            url: "http://localhost:3000/contacts?id=eq."+id
        });
    }
};

module.exports = Contacts;