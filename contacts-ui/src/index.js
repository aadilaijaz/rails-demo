var m = require("mithril");
var List = require("./views/list");
var New = require("./views/new");
var Edit = require("./views/edit");


var root = document.body;

m.route(root, "/list", {
    "/list": List,
    "/new": New,
    "/edit/:id": Edit
});
