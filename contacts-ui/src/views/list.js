var m = require("mithril");
var Contacts = require("../models/contacts")


module.exports = {
    oninit: Contacts.loadList,
    view: function(){
        var ui_components = [
            m("h1", "Contacts List")
        ]
        if(Contacts.list.length !== 0){

            var contact_list = []
            Contacts.list.forEach(function(c){
                contact_list.push(m("tr", [
                    m("td", c.first_name + ' ' + c.last_name),
                    m("td", c.phone),
                    m("td", c.location),
                    m("td", [
                        m("a", {href: "#!/edit/"+c.id}, "Edit"),
                        m.trust(" "),
                        m("a", {href: "#", onclick: function(){
                            Contacts.delete(c.id);
                        }}, "Delete")
                    ])
                ]));
            });
            
            var c_table = m("table.table", [
                m("tr", [
                    m("th", "Name"),
                    m("th", "Phone"),
                    m("th", "Location"),
                    m("th", "Actions")
                ]),
                contact_list
            ]);

            ui_components.push(c_table);
        } else {
            ui_components.push(m("p", "No contacts found"));
        }

        ui_components.push(m("a", {href: "#!/new"}, "Add New Contact"));

        return ui_components;
    }
};