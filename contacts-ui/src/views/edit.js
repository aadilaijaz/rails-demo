var m = require("mithril");
var Contacts = require("../models/contacts")


module.exports = {
    oninit: function(vnode){
        Contacts.loadSingle(vnode.attrs.id);
    },
    view: function() {
        var ui_components = [
            m("h1", "Edit Contact"),
            m("form", {
                onsubmit: function(e){
                    e.preventDefault();
                    Contacts.update();
                    setTimeout(function(){ m.route.set("/list"); }, 1000)
                }
            }, [
                m("p", [
                    m("label[for=first_name]", "First Name"),
                    m("br"),
                    m("input#first_name[type=text][name=first_name]", {
                        oninput: function(e){ Contacts.singleContact.first_name = e.target.value; },
                        value: Contacts.singleContact.first_name
                    })
                ]),
                m("p", [
                    m("label[for=last_name]", "Last Name"),
                    m("br"),
                    m("input#last_name[type=text][name=last_name]", {
                        oninput: function(e){ Contacts.singleContact.last_name = e.target.value; },
                        value: Contacts.singleContact.last_name
                    })
                ]),
                m("p", [
                    m("label[for=phone]", "Phone"),
                    m("br"),
                    m("input#phone[type=text][name=phone]", {
                        oninput: function(e){ Contacts.singleContact.phone = e.target.value; },
                        value: Contacts.singleContact.phone
                    })
                ]),
                m("p", [
                    m("label[for=location]", "Location"),
                    m("br"),
                    m("input#location[type=text][name=location]", {
                        oninput: function(e){ Contacts.singleContact.location = e.target.value; },
                        value: Contacts.singleContact.location
                    })
                ]),
                m("p", [
                    m("input[type=submit]", {value: "Save"})
                ])
            ]),
            m("a", {href: "#!/list"}, "Back")
        ];

        return ui_components;
    }
};