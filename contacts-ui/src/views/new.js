var m = require("mithril");
var Contacts = require("../models/contacts")


module.exports = {
    oninit: function(){
        Contacts.postData = {};
    },
    view: function(){
        var ui_components = [
            m("h1", "New Contact"),
            m("form", {
                onsubmit: function(e){
                    e.preventDefault();
                    Contacts.save();
                    setTimeout(function(){ m.route.set("/list"); }, 1000)
                }
            }, [
                m("p", [
                    m("label[for=first_name]", "First Name"),
                    m("br"),
                    m("input#first_name[type=text][name=first_name]", {
                        oninput: function(e){ Contacts.postData.first_name = e.target.value; }
                    })
                ]),
                m("p", [
                    m("label[for=last_name]", "Last Name"),
                    m("br"),
                    m("input#last_name[type=text][name=last_name]", {
                        oninput: function(e){ Contacts.postData.last_name = e.target.value; }
                    })
                ]),
                m("p", [
                    m("label[for=phone]", "Phone"),
                    m("br"),
                    m("input#phone[type=text][name=phone]", {
                        oninput: function(e){ Contacts.postData.phone = e.target.value; }
                    })
                ]),
                m("p", [
                    m("label[for=location]", "Location"),
                    m("br"),
                    m("input#location[type=text][name=location]", {
                        oninput: function(e){ Contacts.postData.location = e.target.value; }
                    })
                ]),
                m("p", [
                    m("input[type=submit]", {value: "Save"})
                ])
            ]),
            m("a", {href: "#!/list"}, "Back")
        ];
        return ui_components;
    }
};